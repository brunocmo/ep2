<em>Bruno Carmo Nunes - 18/0117548</em>

Versão do kernel linux 5.2.17-200.fc30.x86_64 executado;

# Como Jogar

1. De o ""git clone https://gitlab.com/brunocmo/ep2.git""
2. Crie um projeto com a pasta ep2 vinculada a ele.
3. Rode o arquivo Main.java na sua IDE para jogar o jogo.
4. Use as setas direcionais para mover a cobra.

Bibliotecas utilizadas:

  * java.awt
  * java.swing
  * java.util
