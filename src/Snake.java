import javax.swing.ImageIcon;
import java.awt.*;

public class Snake {

        private int posicao_x;
        private int posicao_y;
        private int width;
        private int height;

        private ImageIcon snakeHead;
        private ImageIcon snakeBody;
        private ImageIcon snakeTail;

        Snake() {
            snakeHead = new ImageIcon("images/head.png");
            snakeBody = new ImageIcon("images/body.png");
            snakeTail = new ImageIcon("images/tail.png");
            width  = 80;
            height = 80;
        }

        Snake (int posicao_x, int posicao_y){

                this.posicao_x = posicao_x;
                this.posicao_y = posicao_y;
                snakeHead = new ImageIcon("images/head.png");
                snakeBody = new ImageIcon("images/body.png");
                snakeTail = new ImageIcon("images/tail.png");
                width  = 80;
                height = 80;

        }

        public ImageIcon getSnakeHead() {
                return snakeHead;
        }

        public ImageIcon getSnakeBody() {
                return snakeBody;
        }

        public ImageIcon getSnakeTail() {
                return snakeTail;
        }

        public void drawBody(Graphics g) {
                g.drawImage(getSnakeBody().getImage(), (posicao_x) * width, (posicao_y) * height , width, height, null);

        }

        public void drawHead(Graphics g) {
                g.drawImage(getSnakeHead().getImage(), (posicao_x) * width, (posicao_y) * height , width, height, null);

        }

        public void drawTail(Graphics g) {
                g.drawImage(getSnakeTail().getImage(), (posicao_x) * width, (posicao_y) * height , width, height, null);
        }

        public int getPosicao_x() {
                return posicao_x;
        }

        public void setPosicao_x(int posicao_x) {
                this.posicao_x = posicao_x;
        }

        public int getPosicao_y() {
                return posicao_y;
        }

        public void setPosicao_y(int posicao_y) {
                this.posicao_y = posicao_y;
        }

}
