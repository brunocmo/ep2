import javax.swing.*;

public class MapaThread extends Thread {
    private Mapa map;
    private boolean running = true;

    public MapaThread(Mapa map) {
        this.map = map;
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            map.paint(map.getGraphics());
        }

    }
}
