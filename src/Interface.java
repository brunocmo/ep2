import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Interface extends JFrame {

    private Mapa map = new Mapa();
    MapaThread updateScreen = new MapaThread(map);

    public Interface() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Center screen

        getContentPane().setLayout(new BorderLayout());
        setTitle("Snake da Carla");
        getContentPane().add("Center", map);

        //Set the width and heigh of principal screen
        setSize(map.RECT_WIDTH * map.getNumberOfRows(), map.RECT_HEIGHT * map.getNumberOfLines());

        setVisible(true);

        updateScreen.start();

        map.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

                if(keyEvent.getKeyCode() == KeyEvent.VK_UP && map.getDirecaoCobraCabeca() != Mapa.DESCE) {
                    map.setDirecaoCobraCabeca(Mapa.SOBE);
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT && map.getDirecaoCobraCabeca() != Mapa.ESQUERDA) {
                    map.setDirecaoCobraCabeca(Mapa.DIREITA);
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN && map.getDirecaoCobraCabeca() != Mapa.SOBE) {
                    map.setDirecaoCobraCabeca(Mapa.DESCE);
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_LEFT && map.getDirecaoCobraCabeca() != Mapa.DIREITA) {
                    map.setDirecaoCobraCabeca(Mapa.ESQUERDA);
                }

            }
            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });





    }

}
