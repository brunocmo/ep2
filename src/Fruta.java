import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Fruta {

    private int posicao_x;
    private int posicao_y;

    private int tempo;
    private int width;
    private int height;
    private ImageIcon frutinha;

    Fruta(Random r) {

        frutinha = new ImageIcon("images/fruta1.jpeg");
        posicao_x = r.nextInt(9)+1;
        posicao_y = r.nextInt(7)+1;
        tempo = 0;
        width  = 80;
        height = 80;
    }


    public ImageIcon getFrutinha() {
        return frutinha;
    }


    public void draw(Graphics g) {
        g.drawImage(getFrutinha().getImage(), (posicao_x) * width, (posicao_y) * height , width, height, null);

    }


    public int getPosicao_x() {
        return posicao_x;
    }

    public void setPosicao_x(int posicao_x) {
        this.posicao_x = posicao_x;
    }

    public int getPosicao_y() {
        return posicao_y;
    }

    public void setPosicao_y(int posicao_y) {
        this.posicao_y = posicao_y;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

}