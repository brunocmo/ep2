import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Mapa extends Canvas {

    public static final int RECT_WIDTH = 80;
    public static final int RECT_HEIGHT = 80;
    public static final int MARGIN = 0;
    public static final int DIREITA = 1;
    public static final int DESCE = 2;
    public static final int ESQUERDA = 3;
    public static final int SOBE = 4;

    private int numberOfRows = 12;
    private int numberOfLines = 8;

    private Snake cobratemporaria;
    private Fruta frutona;
    ArrayList<Snake> cobra2 = new ArrayList<Snake>();


    ArrayList<Fruta> frutas = new ArrayList<Fruta>();


    private int direcaoCobraCabeca = DIREITA;


    private int animationCounter = 0;
    private int animationCounterDirection = 0;
    private int contadordetamanho = 0;

    private Random r = new Random();

    private int x_pos_ant = 1;
    private int y_pos_ant = 3;
    private int x_pos = 1;
    private int y_pos = 1;

    @Override
    public void paint(Graphics g) {

        if (animationCounter < 3) {
            animationCounter++;
        } else animationCounter = 0;


        ImageIcon icon = new ImageIcon("images/grama0.png");

        ImageIcon parede = new ImageIcon("images/wall.png");

        final Image img = icon.getImage();
        final Image pad = parede.getImage();

        for( int i = 0 ; i < numberOfRows; i++ ) {
            for(int j = 0; j < numberOfLines; j++) {

//                if( animationCounterDirection == 0) {
                    g.drawImage(img, i * RECT_WIDTH + MARGIN, j * RECT_HEIGHT + MARGIN, RECT_WIDTH, RECT_HEIGHT, null);
//                }else {
//
//                    if(i != x_pos && j != y_pos) {
//                        g.drawImage(img, i * RECT_WIDTH + MARGIN, j * RECT_HEIGHT + MARGIN, RECT_WIDTH, RECT_HEIGHT, null);
//                    }
//
//                    for(Snake a : cobra2){
//                        if( !(i == a.getPosicao_x() && !(j == a.getPosicao_y()))){
//                            g.drawImage(img, i * RECT_WIDTH + MARGIN, j * RECT_HEIGHT + MARGIN, RECT_WIDTH, RECT_HEIGHT, null);
//                        }
//                    }
//                }
            }
        }

        g.drawImage(pad, 6 * RECT_WIDTH + MARGIN, 2 * RECT_HEIGHT + MARGIN, RECT_WIDTH, RECT_HEIGHT, null);
        g.drawImage(pad, 6 * RECT_WIDTH + MARGIN, 3 * RECT_HEIGHT + MARGIN, RECT_WIDTH, RECT_HEIGHT, null);
        g.drawImage(pad, 6 * RECT_WIDTH + MARGIN, 4 * RECT_HEIGHT + MARGIN, RECT_WIDTH, RECT_HEIGHT, null);

        if(animationCounter == 2) {

                if(contadordetamanho == cobra2.size()) {
                    contadordetamanho = 0;
                } else contadordetamanho++;

                x_pos_ant = x_pos;
                y_pos_ant = y_pos;


            if (direcaoCobraCabeca == SOBE) {

                   y_pos--;

                } else if (direcaoCobraCabeca == DIREITA){

                   x_pos++;
                } else if (direcaoCobraCabeca == ESQUERDA){

                   x_pos--;

                } else if (direcaoCobraCabeca == DESCE) {

                   y_pos++;
                }

                Snake cabecanova = new Snake(x_pos,y_pos);

                for (Snake a : cobra2 ) {
                    if(x_pos == a.getPosicao_x() && y_pos == a.getPosicao_y()){
                        gameOver(g);
                    }
                }

                cabecanova.drawHead(g);

            if( frutas.isEmpty() ){
                frutona = new Fruta(r);
                frutas.add(frutona);
            }else {
                frutas.get(0).draw(g);
                frutas.get(0).setTempo(frutas.get(0).getTempo() + 1);

            }

                if(y_pos == frutas.get(0).getPosicao_y() && x_pos == frutas.get(0).getPosicao_x()){

                    if(cobra2.isEmpty()) {
                        cobratemporaria = new Snake(x_pos_ant, y_pos_ant);
                        cobra2.add(cobratemporaria);
                        frutas.clear();

                    }else {
                        cobratemporaria = new Snake(cobra2.get((cobra2.size()-1)).getPosicao_x(),cobra2.get((cobra2.size()-1)).getPosicao_y());
                        cobra2.add(cobratemporaria);
                        frutas.clear();

                    }
                }

                if(!cobra2.isEmpty()) {



                    for(int a = 0; a < cobra2.size(); a++){

                        cobra2.get(a).drawBody(g);
                        if(contadordetamanho  == a){
                            cobra2.get(a).setPosicao_x(x_pos);
                            cobra2.get(a).setPosicao_y(y_pos);
                        }


//                        if( cobra2.size() == 1) {
//                            cobra2.get(a).setPosicao_x(x_pos);
//                            cobra2.get(a).setPosicao_y(y_pos);
//                        }else {
//                            if (cobra2.size()-1 != a) {
//                                cobra2.get(a+1).setPosicao_x(cobra2.get(a).getPosicao_x());
//                                cobra2.get(a+1).setPosicao_y(cobra2.get(a).getPosicao_y());
//                                cobra2.get(a).setPosicao_x(x_pos);
//                                cobra2.get(a).setPosicao_y(y_pos);
//
//                            } else {
//                                cobra2.get(a).setPosicao_x(x_pos);
//                                cobra2.get(a).setPosicao_y(y_pos);
//                            }
//
//                        }


                    }

                }

            if (animationCounterDirection == 0) {
                JOptionPane.showMessageDialog(null, "Aperte OK para começar");

            }

            animationCounterDirection++;

            if( !(frutas.isEmpty()) ){
                    if(frutas.get(0).getTempo() == 16) {
                    frutas.clear();
                    }
                }

                if (x_pos == 6 && (y_pos == 2 || y_pos == 3 || y_pos == 4)) {
                    gameOver(g);
                }

                if(x_pos > 11 || x_pos < 0 || y_pos > 8 || y_pos < 0){
                    gameOver(g);
                }

        }

    }


    public int getNumberOfRows() {
        return numberOfRows;
    }

    public int getNumberOfLines() {
        return numberOfLines;
    }


    public void setDirecaoCobraCabeca(int direcaoCobraCabeca) {
        this.direcaoCobraCabeca = direcaoCobraCabeca;
    }


    public int getDirecaoCobraCabeca() {
        return direcaoCobraCabeca;
    }

    private void gameOver (Graphics g) {

        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 42);
        FontMetrics metr = getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg,((RECT_WIDTH * numberOfLines) - metr.stringWidth(msg)) / 2, (RECT_HEIGHT*numberOfRows) / 2);

        JOptionPane.showMessageDialog(null, "GAME OVER");

        System.exit(0);

    }

}
